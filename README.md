# Custom iScroll4

Custom version of iScroll by Matteo Spinelli [http://cubiq.org] including some useful patches from users.

## Including
#### Draggable Scrollbars
 - draggableScrollbars: true
 - draggableScrollbars: false (default)

#### Scroll Offset
Max touch/click offset to start the scroll event.

 - xScrollOffset: int (default: 6)
 - yScrollOffset: int (default: 6)
